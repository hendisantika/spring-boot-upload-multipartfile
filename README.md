# Demo Upload

Run this command :

`gradle clean bootRun`

### Screenshot

Upload File

![Upload File](img/upload.png "Upload File")

Success Page

![Success Page](img/success.png "Success Page")

List Uploaded Files

![List Uploaded Files](img/list.png "List Uploaded Files")